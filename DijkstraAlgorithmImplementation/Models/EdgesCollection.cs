﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DijkstraAlgorithmImplementation.Models
{
    public class EdgesCollection : List<Edge>
    {
        // Constructor
        public EdgesCollection()
            : base()
        { }

        // Methods
        public Edge FindEdgeWithNodes(Node n1, Node n2)
        {
            return this.FirstOrDefault(e => (e.N1.Name == n1.Name && e.N2.Name == n2.Name) || (e.N2.Name == n1.Name && e.N1.Name == n2.Name));
        }
    }
}
