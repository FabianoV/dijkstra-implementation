﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DijkstraAlgorithmImplementation.Models
{
    public class Graph
    {
        public List<Node> Nodes { get; set; }
        public EdgesCollection Edges { get; set; }
    }
}
