﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DijkstraAlgorithmImplementation.Models
{
    public class Edge
    {
        // Properties
        public Node N1 { get; set; }
        public Node N2 { get; set; }
        public int Weight { get; set; }
    
        // Constructor
        public Edge(Node n1, Node n2, int weight)
        {
            N1 = n1;
            N2 = n2;
            Weight = weight;
        }

        // Methods
        public override string ToString()
        {
            return $"Edge: {N1.Name}---({Weight})---{N2.Name}";
        }
    }
}
