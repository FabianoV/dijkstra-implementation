﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DijkstraAlgorithmImplementation.Models
{
    public class Node
    {
        // Properties
        public string Name { get; set; }

        // For Dijkstra path result
        public int Distance { get; set; }
        public Node Previous { get; set; }

        // Constructor
        public Node(string name)
        {
            Name = name;
        }

        // Methods
        public override string ToString()
        {
            return $"Node: {Name}";
        }
    }
}
