﻿using DijkstraAlgorithmImplementation.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DijkstraAlgorithmImplementation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var A = new Node("A");
            var B = new Node("B");
            var C = new Node("C");
            var D = new Node("D");
            var E = new Node("E");
            var F = new Node("F");

            // Look at graph preview in Content/graph_preview.png
            Graph g = new Graph()
            {
                Nodes = new List<Node>() { A, B, C, D, E, F },
                Edges = new EdgesCollection()
                {
                    new Edge(A, B, 4),
                    new Edge(A, C, 4),
                    new Edge(B, C, 2),
                    new Edge(C, D, 3),
                    new Edge(C, E, 1),
                    new Edge(D, F, 2),
                    new Edge(E, F, 3),
                    new Edge(C, F, 6),
                }
            };

            var dijkstra = new DijkstraAlgorithmImplementation();
            dijkstra.Run(g, A);

            Console.WriteLine("Source Node: " + A);

            Console.WriteLine("Nodes:");
            foreach (var node in g.Nodes)
            {
                Console.WriteLine(node + $" Distance: {node.Distance}, Previous Node: {node.Previous}");
            }
        }
    }
}
