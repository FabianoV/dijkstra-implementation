﻿using DijkstraAlgorithmImplementation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DijkstraAlgorithmImplementation
{
    public class DijkstraAlgorithmImplementation
    {
        public void Run(Graph G, Node source)
        {
            List<Node> Q = new List<Node>();

            foreach (var n in G.Nodes)
            {
                n.Distance = int.MaxValue;
                n.Previous = null;
                Q.Add(n);
            }

            source.Distance = 0;

            while (Q.Count > 0)
            {
                var uMin = Q.Min(n => n.Distance);
                var u = Q.First(n => n.Distance == uMin);
                Q.Remove(u);

                foreach (var v in GetNeightbours(G, u, Q))
                {
                    var edge = G.Edges.FindEdgeWithNodes(v, u);
                    int altDistance = u.Distance + edge.Weight;
                    if (altDistance < v.Distance)
                    {
                        v.Distance = altDistance;
                        v.Previous = u;
                    }
                }
            }
        }

        /// <summary>
        /// Find all neightbour nodes in graph g, that are connected to node u and second point of edge is in Q collection.
        /// </summary>
        private List<Node> GetNeightbours(Graph g, Node u, List<Node> Q)
        {
            List<Node> neightbours = new List<Node>();

            foreach (var edge in g.Edges)
            {
                if (edge.N1.Name == u.Name || edge.N2.Name == u.Name)
                {
                    var secondNode = (edge.N1.Name == u.Name) ? edge.N2 : edge.N1;

                    if (Q.Any(n => secondNode.Name == n.Name))
                    {
                        neightbours.Add(secondNode);
                    }
                }
            }

            return neightbours;
        }
    }
}
